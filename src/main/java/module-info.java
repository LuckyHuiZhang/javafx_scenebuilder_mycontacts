module example.scenebuilder {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.xml;


    opens example.scenebuilder to javafx.fxml;
    exports example.scenebuilder;
    //exports new package
    exports datamodel;
}