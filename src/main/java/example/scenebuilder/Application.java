package example.scenebuilder;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Application extends javafx.application.Application {
    @Override
    public void start(Stage stage) throws IOException {
        //version 1
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource("view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("My Contacts");
        stage.setScene(scene);
        stage.show();
        /*version 2
//        Parent root = FXMLLoader.load(getClass().getResource("view.fxml"));
//        stage.setTitle("My Contacts");
//        stage.setScene(new Scene(root,600,400));
//        stage.show();
         */
    }

    public static void main(String[] args) {
        launch();
    }
}